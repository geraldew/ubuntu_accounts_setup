#!/bin/bash
echo $0
echo "Setting up for usage."
echo "Making folders"
mkdir -p ~/applctn
cd ~/applctn
echo "Making Clone from GitLab"
git clone https://gitlab.com/geraldew/ubuntu_accounts_setup.git
cd ~/applctn/ubuntu_accounts_setup
echo "Pulling from GitLab"
git checkout main
git fetch origin main
git reset --hard FETCH_HEAD
git clean -df
echo "Completed"
echo "Your scripts are:"
ls *.bash
