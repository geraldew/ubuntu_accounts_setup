# ubuntu_accounts_setup

This is just some bash scripting to enable repeated user account setup for Xubuntu Linux installations.
It may well work for other situations, I'm only testing it on my own. 

The major concept is for this to provide both a method and an actual example of something that can be copied in to a freshly installed Xubuntu Linux setup so as to:
- create a stock set of user accounts
- preset that each account will have a designated avatar image for the LightDM login screen
- preset that each account will have a designated background image for the LightDM login screen

## Current status

Current status: in short, it works! But see the following caveats.

See the [wiki](https://gitlab.com/geraldew/ubuntu_accounts_setup/-/wikis/home) for a screenshot.

## Risk Caveats

As merely some bash scripting for making some settings, it is neither robust nore secure.

In particular it does not do much vetting of the settings that it makes, and considering the idea is to run it using `sudo` that introduces *risks* if something is awry with the values in the list data file.

## Feature Caveats

Note that it does not enable nor set passwords for the accounts, those then have to be done manually (or by some other method).

Note that sets the avatars and backgrounds for LightDM and that these are independent of what the users may set inside their own accounts.

It currently assumes that the file references are local to the folder from which it runs, this means that if the list file has references to paths and files outside of those, then those will be merely replicated and added to the expected destination paths. Consequently the file references thus set will be _wrong_ and therefor simply not work.

So for now, just copy all your image files into the working directory and have the list file quite their names not their paths. 

## Why write this?

For a variety of reasons I make and test various setups of Xubuntu, each with the same set of accounts - notably being the same name and id number pairs.

So having a routine way to add the user accounts after the initial setup is a worthwhile convenience. In particular I quite like having the login manager show a unique image and background combination for each user account as it makes logging in to the required account happen with less concentration and frankly is simply pleasant.

## Brief

Essentially the scripts perform three actions:
- create the desired user accounts
- for each account, set a desired "avatar" image for use in the login screen (aka the Display Manager) 
- for each account, set a desired background image for use in the login screen (aka the Display Manager) 


Apart from doing the job, the main goal is to have no dependencies beyond what is reliably there in a fresh Xubuntu install.

In particular, between Xubuntu 20.04 and Xubuntu 22.04 permission changes required a change of method. It used to be enough to just icon files in the home folder of each user - and that LightDM would pick up the wallpaper of the first workspace of each user. Now however both of those are not visible to LightDM. So instead we are settings things for the `AccountsService` to use.

It is assumed that the script or scripts will be run via sudo, meaning that there is already an "admin" class account that was made during the setup.

As it happens, placing this file set on a public Git server - such as GitLab - is an easy way to distribute it, especially for testing.

Currently the tesitng is being done with VirtualBox so that a snapshot state can be reverted to re-test, as this is cleaner and neater than having a undo script.

Hence the nature of this respository is partly tuned to the convenience of testing.

NOTE therefore! What you pull from this repository might be a work in progress rather than a stable state. This is an open sharing, not a product!

For added safety, the main scripts are always stored such that there is an `exit` command that prevents them from doing anything. Therefore after fetching (or copying into place) they need to be manually edited to remove that `exit` line.

## Dependencies

Currently the only dependency is that `crudini` is already installed. See the file `setup_installs_base.bash` for how to install that.

You only need to pre-install `git` if you are using it to pull the files down from GitLab. If you download the files yourself then you don't need that.

## Usage Sequence

- Copy the files from this repository
    - if you want to do that via git then:
    - download just the file `setup_setup.bash`
    - have a terminal session with the current directory as one where a new directory will be made
    - give the command `bash setup_setup.bash`
- have a terminal session with the current directory as one where the files are
- edit the file `stockusers.txt` to suit your needs
- to make the user accounts, give the command `bash stockusers.bash` - it will tell to edit out the first `exit` line  
- to set the avatars and backgrounds, give the command `bash set_login_avatars_backgrounds.bash` - it will tell to edit out the first `exit` line  

## Detail

The main idea is to base operations around a text data file `stockusers.txt` which has comma separated values per line:
- the name of the account
- the id number for that account
- the path and fileanme of the avatar image file - note that this must not contain spaces or other problem characters
- the path and fileanme of the background image file - note that this must not contain spaces or other problem characters

Note: I do not intend covering the requirements and limitations for those image files. See elsewhere. 

### Account creation

After all the handholding is done, the accounts are setup by the line

``` bash
        adduser --uid $uid --disabled-password --gecos "" $unm
``` 

### Login screen and Account wallpapers

Prior to Xubuntu 22.04 it happened that:
- the Xubuntu installation uses LightDM as the "Display Manager" i.e. the login screen
- it happened that LightDM will show each users first workspace wallpaper as a background as you select the account to log into
- if a user account had a `.face` (or a `.face.icon` ) file in their home folder 
- this worked because the permissions used by Xubuntu allowe LightDM to read those files.

The new method requires placing image files and making text settings in a location used by `AccountsService` and this is done my making folders, copying files and using the program `crudini` which can modify a text file in the archaic config (or "ini" if you have old Windows experience) file style.

See the script for the specifics, but the general location is `/var/lib/AccountsService/`

## Files

- `README.md` = what you're reading


- `stockusers.txt` = the text data file of user and id pairs
- `stockusers.bash` = this is the script that will create the accounts specified in the data file
- `stockusers_which_exist.bash` = a script to check which of the specified accounts already exist
- `set_login_avatars_backgrounds.bash` = the current script for setting avatars for use with Xubuntu 22.04

- `avatar_alpha.png`
- `avatar_beta.png`
- `avatar_gamma.png`
- `avatar_delta.png`

- `wallpaper_alpha.jpg`
- `wallpaper_beta.jpg`
- `wallpaper_gamma.jpg`
- `wallpaper_delta.jpg`

But it is first necessary to manually download two of these files:
- `setup_installs_base.bash` - this has the commands to install git and crudini which are needed a) git to download the files, b) crudini used to make settings
- `setup_setup.bash` = this is a script that will pull the rest of the files from GitLab and move to the created folder - hence this can be simply downloaded manually from GitLab into the admin account home folder and then executed - i.e. with `bash setup_setup.bash` 


## Software License

Frankly I'm not currently of the view that there is enough content here for copyright to be meaningful, so no explicit software license is being declared.

## Links

Here are some of the links I used to work out my solution. In the end it was a bit from here and a bit from there.

- [How can I have my login screen use the same wallpaper as my desktop?](https://askubuntu.com/questions/517602/how-can-i-have-my-login-screen-use-the-same-wallpaper-as-my-desktop)
- [Unable to customise LightDM login background](https://bbs.archlinux.org/viewtopic.php?id=282444)
- [LightDM](https://wiki.archlinux.org/title/LightDM)
- [Permission issues with /var/lib/AccountsService/users/<username>](https://github.com/canonical/lightdm/issues/117)
- [Can't set user picture in Xubuntu 22.04](https://askubuntu.com/questions/1418888/cant-set-user-picture-in-xubuntu-22-04)
- [Fix for lightdm background keeps changing on login or logout](https://arcolinux.com/fix-for-lightdm-background-keeps-changing-on-login-or-logout/)

