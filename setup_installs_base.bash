#!/bin/bash
echo "Running script" $0
echo "This is part of the geraldew stock setup scripts for Ubuntu and derivatives"
echo
echo "This one provides the commands to add various software packages"
echo "Core package installations"
echo 
echo "This will need to be run as sudo"
echo
echo "Edit to suit the situation then comment out the first exit"
exit
# Notes:
# This set is now preset for use with Ubuntu 22.04 and derivatives
# for Ubuntu 22.04 and derivatives
# - for Xubuntu, synaptic is pre-installed
# - grub-customizer is not in the repositories
#
apt install git
apt install crudini
echo "Setup complete"
echo $0 done
exit
