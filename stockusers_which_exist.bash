#!/bin/bash
echo "Running script" $0
echo "This is part of the stock setup scripts for Ubuntu and derivatives"
echo
echo "This one harmlessly checks the users listed in stockusers.txt for existence"
echo "So as to guide you in editing that list before running the stockusers bash script"
echo 
# Notes:
#
echo "Checking through stockusers.txt :"
while IFS=, read unm uid uavafile ubackfil;do
	echo $unm $uid
	if id "$unm" &>/dev/null; then
		echo 'user found'
	else
		echo 'user not found'
		fi
	done < stockusers.txt
echo "Displaying stockusers.txt :"
cat stockusers.txt
echo
echo $0 done
