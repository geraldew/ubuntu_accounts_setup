#!/bin/bash
echo "Running script" $0
echo "This is part of the stock setup scripts for Ubuntu and derivatives"
echo
echo "This a script to automate the adding of several stock user accounts"
echo "as listed in the file stockusers.txt which has comma separated values of:"
echo "  username,useridnum,avatarfilename,loginbackgroundfilename"
echo "The accounts are set as disabled with no passwords,"
echo "leaving that for manual admin attention (or other methods)."
echo
echo "It also creates a group called homeshare and makes all the user accounts members"
echo "and makes a folder /home/sharelocal for use by them all."
echo
echo "This script will need to be run as sudo"
echo "Usage:"
echo "sudo bash $0"
echo
echo "Now edit this script to comment out the first exit (by putting a # in front of it)"
exit
# Notes:
# 
echo $0 doing
# homeshare
echo "Making group: homeshare"
addgroup --gid 1100 homeshare
echo "Adding the current user into homeshare group"
adduser ${SUDO_USER:-$USER} homeshare
echo "Making folder /home/sharelocal"
mkdir -p /home/sharelocal
chgrp -R homeshare /home/sharelocal
chmod -R g+w /home/sharelocal
# user list
echo "Looping through the userlist as found in stockusers.txt"
while IFS=, read unm uid uavafile ubackfil;do
  if id "$unm" &>/dev/null; then
    echo $unm "account already exists"
  else
    if [ -z "$unm" ]; then
      echo "No account name specified!"
    else
      if [ -z "$uid" ]; then
        echo "No account id specified!"
      else
        echo $unm $uid
        echo "Making user: $uid $unm"
        adduser --uid $uid --disabled-password --gecos "" $unm
        fi
      fi
    fi
  echo "Adding user $unm into homeshare group"
  adduser $unm homeshare
  done < stockusers.txt
echo "You should now go into the admin GUI and for each user set a password to enable the account."
echo $0 done
