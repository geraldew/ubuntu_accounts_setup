#!/bin/bash
echo "Running script" $0
echo "Set User image files as avatars for Xubuntu 22.04"
echo "This will need to be run with sudo"
echo "It assumes that crudini has been installed ( e.g. sudo apt install crudini )"
echo
echo "This a script to automate the adding of several stock user accounts"
echo "as listed in the file stockusers.txt which has comma separated values of:"
echo "  username,useridnum,avatarfilename,loginbackgroundfilename"
echo "Currently the filenames must not have spaces or other problem characters."
echo
echo "It currently skips any non-existing accounts, comment out the if else echo fi lines"
echo "to have it prepare everything listed in the stockusers.txt file."
echo
echo "Now edit this script to comment out the first exit (by putting a # in front of it)"
exit
#
echo "Looping through the userlist as found in stockusers.txt"
while IFS=, read unm uid uavafile ubackfil;do
  echo $unm $uid $uavafile $ubackfil
  if id "$unm" &>/dev/null; then
    if [ -z "$uavafile" ]; then
      echo "No avatar image specified!"
    else
      echo "Setting up Avatar"
      # for LightDM in Ubuntu 20.04 and eariler
      echo "Set in /home/$unm as .face"
      cp $uavafile /home/$unm/.face
      chown $unm /home/$unm/.face
      chgrp $unm /home/$unm/.face
      # for SDDM in Ubuntu 20.04 and eariler
      echo "Set in /home/$unm as .face.icon"
      cp $uavafile /home/$unm/.face.icon
      chown $unm /home/$unm/.face.icon
      chgrp $unm /home/$unm/.face.icon
      # for Ubuntu 22.04
      echo "additional for Ubuntu 22.04"
      echo "Set for AccountsService"
      cp $uavafile /var/lib/AccountsService/icons
      # chmod 644 /var/lib/AccountsService/icons/$uavafile
      echo "do setting in AccountsService" 
      touch /var/lib/AccountsService/users/$unm
      crudini --set /var/lib/AccountsService/users/$unm User Icon "/var/lib/AccountsService/icons/$uavafile"
      fi
    #
    if [ -z "$ubackfil" ]; then
      echo "No background image specified!"
    else
      echo "Setting for Login background"
      echo "ensure there is a location for the backgrounds"
      mkdir -p /var/lib/AccountsService/backgrounds
      # chmod 777 /var/lib/AccountsService/backgrounds
      #
      echo "copy background image into place"
      cp $ubackfil /var/lib/AccountsService/backgrounds
      # chmod 644 /var/lib/AccountsService/backgrounds/$ubackfil
      echo "set user background to that file"
      crudini --set /var/lib/AccountsService/users/$unm org.freedesktop.DisplayManager.AccountsService BackgroundFile "'/var/lib/AccountsService/backgrounds/$ubackfil'"
      fi
  else
    echo $unm "no such account exists"
    fi
  done < stockusers.txt
echo "Done! $0"
